# How-To

**To simplify the install process there is an official Yeoman generator available here:** [generator-razor](https://www.npmjs.com/package/generator-razor)

You need to have node.js and yeoman installed globally in order to use the generator.

1. After Yeoman is done, go to the extension manager and install the razor extension. It will also install all its dependencies and files to your TYPO3 installation
2. After successfull installation, go to the TYPO3 Install Tool and click on Important actions -\> Compare current database with specification and run the DB analyzer until there's no errors and warnings anymore
3. Set Local, Staging, Live and Feature settings in the razor settings in the extension manager if needed
![Extension Manager](screenshot-em.jpg)
4. Go to **fileadmin/razor/TypoScript/constants/StandardConfig.ts** and edit the Local, Staging, Live and Feature setting here, too **or** do it through the Constant Editor in the TYPO3 backend itself (not versioned through git though)
![Extension Manager](screenshot-ts.jpg)
5. Run `./razor.sh` in the main folder
6. Reload the backend
7. Enjoy!