# Introduction

The razor distribution was created to serve as a full-featured TYPO3 distribution build on top of TYPO3 coding standards and Bootstrap 4 so that you, the user has the maximum benefit of using this distribution.

A lot of time (mostly free time aside from work) went into this project so that the foundation is stable but also easy to use for anyone with some TYPO3 knowledge. A lot of websites are already running the razor distribution (or formerly t3med) and it is in constant development to improve it even more.

I hope you like what I created and use it for awesome projects in the future - if you do, just drop me a line: [rafu1987@gmail.com](mailto:rafu1987@gmail.com) :)

Read the requirements and this whole documentation carefully though!

Have fun!

**You can find the official repository with an issue tracker here:** [razor Bitbucket](https://bitbucket.org/rafu1987/razor)

Powered and maintained by [@rafu1987](http://bitbucket.org/rafu1987/)

[![Documentation Status](https://readthedocs.org/projects/razor/badge/?version=master)](http://razor.readthedocs.io/en/master/?badge=master)