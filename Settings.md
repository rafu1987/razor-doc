# Settings

These are the TypoScript (constants) settings for razor, prefixed by **razor.**

| Title         | | Explanation | Default value |
| --------------| --------------------- | ----------- |
|basedomain||||
||local||testproject.localhost|
||staging||staging|
||live||www.testproject.live|
||feature||testproject.feature|
||protocol||http|
|site||||
||projectName||Test Project|
||showProjectNameInMenu||1|
||breadcrumb||1|
||breadcrumbOnHomepage||0|
||search||1|
|colors||||
||tile||#000000|
||theme||#ffffff|
|bootstrap||||
||cols||12|
||defaultSize|||
||defaultOffset|||
||borderRadius||1|
|fontAwesome||||
||activate||1|
||listIcon||\f0c8|
|pid||||
||searchPid||13|
|seo||||
||gaCodeLive||UA-XXXXXXXX-Y|
||googlePlus|||
|image||||
||galleryEffect||fade|
||responsiveImages||0|
||tinyPngApiKey||G4a62COu_asTIa-9sUsNn_oN-SpqOspP|
|powermail||||
||powermailEmail||no-reply@razor.de|
||siteKey|||
||secretKey|||
|cookie||||
||activate||1|
|news||||
||activateDisqus||0|
||disqusShortname|||
||lightbox||1|
||rss||1|
|misc||||
||wow||0|
||responsiveTables||1|
||hyphenator||1|
||retinaJs||0|
||hoverCss||0|
||normalizeCss||0|
||fileIcons||fileadmin/razor/Icons/Filetypes/|
||copyright||TYPO3 implementation by @razor|
||browserSyncVersion||2.11.1|
||languageDetection||0|
||deactivateScriptmergerLocal||0|
||mainCss||fileadmin/razor/Dist/Main.css|
||mainJs||fileadmin/razor/Dist/Main.js|