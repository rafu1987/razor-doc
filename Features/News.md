# News

razor ships with custom news templates for the popular news extension ([http://typo3.org/extensions/repository/view/news)](http://typo3.org/extensions/repository/view/news)). Additionally we added an own **news detail doktype** to provide some configuration for news detail pages.

![News detail page doktype](news-detail.jpg)

## Problems with translated news

We added a custom ViewHelper to filter news records in the list view to only match the news that are available in that language. Default news behaviour would be if i.e. 2 news records exist in German (default language) and only one is translated in English, both news would still show up on the English page.

## Fields

razor includes some new fields to the news table:

| Title         | Field                 | Explanation |
| --------------| --------------------- | ----------- |
| News content     | newscontent | Add flexible news content elements |
| Opengraph image     | ogimage | Add a dedicated opengraph image |
| City     | city | New field for a city |
| Datetime end     | datetimeend | New field to define a end date |
| Date freetext     | datefreetext | A freetext field for dates |
| Author     | newsauthor | Add a news author |