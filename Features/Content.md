# Content

razor includes a few new fields to the tt_content table:

## Fields

| Title         | Field                 | Explanation |
| --------------| --------------------- | ----------- |
| Visibilty     | tx\_razor\_visibility | This option adds the possibility to add the Bootstrap visibility classes (http://getbootstrap.com/css/#responsive-utilities) to each content element. |
| wow.js     | tx\_razor\_wow | Add a wow.js effect to your content element. |
| Classes     | tx\_razor\_classes | Add some custom CSS classes to your content element. |
| Tablestyles | tx\_razor\_tablestyles | Add custom tablestyles |
| Background | tx\_razor\_gridbackground | Add a field to add background images to grid elements |
| Header style | header_style | Adds a field for header styles |
| Header utility | header_utility | Adds a new field for header utility classes |
| Header position | header_position | Reintroduces the header position field |