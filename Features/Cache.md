# Cache

razor has some additional cache functions:

1. When clearing the frontend cache (green lightning bolt in backend) the dynamic razor.css file is deleted.
2. When clearing all caches (red lightning bolt) all cf\_\* tables are cleared. Also the vhs and opcode caches and all temp files are cleared.
3. When saving razor records **Color** *(tx\_razor\_domain\_model\_color)* and **Padding** *(tx\_razor\_domain\_model\_padding)* the dynamic razor.css file is deleted as it has to be generated again by the frontend. That saves you the trouble of deleting the cache every time you change one of these records.