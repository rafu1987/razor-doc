# Debug

razor ships with an own debug class, to debug all kinds of things when developing in the backend or frontend. This is an example how to use the Debug class:

``` php
<?php
namespace VENDOR\Extensionname\Controller;

use RZ\Razor\Utility\Debug;

class yourClass
{
  public function yourFunction()
  {
    $debugVar = 'This is a debug';
    $query    = $table->getQuery();

    // Regular debug
    Debug::debug($debugVar);

    // Debug in popup
    Debug::debugPopup($debugVar);

    // Debug wrapped in <pre>
    Debug::debugPre($debugVar);

    // Debug to file
    Debug::debugFile($debugVar);
    
    // Debug to file
    Debug::debugFluid($debugVar);

    // Debug query
    Debug::debugQuery($query);
  }
}
```