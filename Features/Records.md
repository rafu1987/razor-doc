# Records

razor ships with some custom records, located in the Storage Folder (PID 2).

## Tables

| Type         | Table                             | Explanation |
| ------------ |-----------------------------------|-------------|
| Colors       | tx\_razor\_domain\_model\_color       | You can define colors with additional CSS to use in various content elements. |
| Effects       | tx\_razor\_domain\_model\_effects       | You can define wow effects. |
| Flexbox       | tx\_razor\_domain\_model\_flexbox       | Add some flexbox utility classes. |
| Headerstyles       | tx\_razor\_domain\_model\_headerstyles       | Add some custom headerstyles. |
| Padding      | tx\_razor\_domain\_model\_padding     | Paddings are used in various content elements to add a space. |
| Sizes        | tx\_razor\_domain\_model\_size        | Sizes are mostly used in Bootstrap buttons and wells. You can add your own sizes depending on your needs and style them with CSS afterwards. |
| States       | tx\_razor\_domain\_model\_state       | States are mostly used in Bootstrap buttons and alerts. You can add your own states depending on your needs and style them with CSS afterwards. |
| Table styles | tx\_razor\_domain\_model\_tablestyles | Table styles are used for the standard TYPO3 table content element to add the Bootstrap classes. You can add your own table styles depending on your needs and style them with CSS afterwards. |
| Utility | tx\_razor\_domain\_model\_utility | Add some utility classes. |
| Visibility | tx\_razor\_domain\_model\_visibiklity | Add some visibility classes. |