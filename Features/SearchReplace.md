# search/replace

razor includes the awesome search/replace tool from Interconnect IT ([https://interconnectit.com/products/search-and-replace-for-wordpress-databases/](https://interconnectit.com/products/search-and-replace-for-wordpress-databases/)) as a backend module to perform easy database updates. It is recommended to deactivate this module once your site is online (through the extension configuration of razor).

![search/replace](search-replace.jpg)