# Powermail

razor ships with custom powermail templates ([http://typo3.org/extensions/repository/view/powermail)](http://typo3.org/extensions/repository/view/powermail)). We also added some functionality to powermail:

## Fields

| Title       | Field             | Explanation |
| ----------- | ----------------- | ----------- |
| Maxlength   | tx\_razor\_powermail\_maxlength | With this option you can add a maxlength to input fields and textareas. |
| Readonly    | tx\_razor\_powermail\_readonly  | You can add a readonly flag to input fields and textareas with this option. |