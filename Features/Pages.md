# Pages

razor includes three new fields to pages:

## Fields

| Title                      | Field             | Explanation |
| -------------------------- | ----------------- | ----------- |
| Do not link in menus       | tx\_razor\_menulink | With this option you can define if a menu item should be linked or not. This is useful for a menu item with submenu items or a breadcrumb menu if a item should just not be linked to a page (that would be empty). |
| facebook image (og:image)  | tx\_razor\_ogimage  | With this field you can define an image for the meta tag og:image (mostly used for facebook). |
| Do link in breadcrumbs  | tx\_razor\_breadcrumb  | Link a page in a breadcrumb menu, even when it is hidden in menus |