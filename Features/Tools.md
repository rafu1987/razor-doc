# Tools

razor ships with a backend module to quickly kickstart a helper extension. Typical use cases would be if you need a custom ViewHelper for your Fluid template or some additional fields for records (i.e. pages).

![Tools](tools.jpg)