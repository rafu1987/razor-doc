# Filter News ViewHelper

## Example usage

```html
<razor:news.filter news="{news}" />
```

### Tag usage example

```html
<razor:news.filter news="NULL" />
```

### Inline usage example

```
{razor:news.filter(news:'NULL')}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| news* | The news object | mixed     |

\* Required argument