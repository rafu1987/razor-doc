# Cols Array ViewHelper

## Example usage

```html
<razor:layout.colImpExp content="{0: 'foo', 1: 'bar'}" row="1" />
```

### Tag usage example

```html
<razor:layout.colImpExp content="[mixed]" row="0" />
```

### Inline usage example

```
{razor:layout.colImpExp(content: [mixed], row: 0)}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| content*   | The content to pass                   | mixed   |
| row        | If it should be added to a row or not | boolean |

\* Required argument