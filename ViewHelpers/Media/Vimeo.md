# Vimeo ViewHelper

### Tag usage example

```html
<razor:media.vimeo title="1" byline="1" portrait="1" color="'00adef'" defer="1" content="1">
	<!-- tag content - may be ignored! -->
</razor:media.vimeo>
```

### Inline usage example

```
{razor:media.vimeo(title: 1, byline: 1, portrait: 1, color: ''00adef'', defer: 1, content: 1)}
```

## Arguments

| Argument   | Description      | Type    |
| -----------| ---------------- | ------- |
| title      | Show the title on the video. Defaults to TRUE.        | boolean   |
| byline      | Show the users byline on the video. Defaults to TRUE.        | boolean   |
| portrait      | Show the users portrait on the video. Defaults to TRUE.        | boolean   |
| color      | Specify the color of the video controls. Defaults to 00adef. Make sure that you dont include the #.        | string   |
| defer      | Defer the video load       | boolean   |
| content      | Content     | string   |

\* Required argument