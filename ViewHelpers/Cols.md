# Cols ViewHelper

## Example usage

```html
<razor:layout.cols cols="12" divider="3" subtrahend="4" />
```

### Tag usage example

```html
<razor:layout.cols cols="NULL" divider="0" subtrahend="0" />
```

### Inline usage example

```
{razor:layout.cols(cols: 'NULL', divider: 0, subtrahend: 0)}
```

## Arguments

| Argument   | Description                               | Type    |
| -----------| ----------------------------------------- | ------- |
| cols*      | The col amount                            | integer |
| divider    | By how much the cols should be devided    | integer |
| subtrahend | By how much the cols should be subtracted | integer |

\* Required argument