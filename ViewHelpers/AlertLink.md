# Alert Link ViewHelper

## Example usage

```html
<razor:utility.alertLink text="This is a text with a link" />
```

### Tag usage example

```html
<razor:utility.alertLink text="NULL" />
```

### Inline usage example

```
{razor:utility.alertLink(text: 'NULL')}
```

## Arguments

| Argument   | Description      | Type    |
| -----------| ---------------- | ------- |
| text*      | Content to parse | string  |

\* Required argument