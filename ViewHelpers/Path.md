# Get file extension ViewHelper

## Example usage

```html
<razor:file.path file="{f:uri.image(src: fileReference.uid, treatIdAsReference: 1)}" />
```

### Tag usage example

```html
<razor:file.path file="NULL" />
```

### Inline usage example

```
{razor:file.path(file:'NULL')}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| file* | The file reference | mixed     |

\* Required argument