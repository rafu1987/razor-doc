# Filesize ViewHelper

## Example usage

```html
<razor:file.size file="fileadmin/razor/Path/To/Image.jpg" />
```

### Tag usage example

```html
<razor:file.size file="NULL" />
```

### Inline usage example

```
{razor:file.size(file:'NULL')}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| file* | The file | string     |

\* Required argument