# Share ViewHelper

## Example usage

```html
<razor:media.share all="1" as="channels"></razor:media.share>
```

### Tag usage example

```html
<razor:media.share all="NULL" as="NULL" channels="NULL" url="NULL" path="" type="" text="" />
```

### Inline usage example

```
{razor:media.share(all:'NULL', as:'NULL', channels:'NULL', url:'NULL', path:'NULL', type:'NULL', text:'NULL')}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| all | Show all share channels | boolean     |
| as* | Variable to use afterwards | string     |
| channels | Array list of channels to display | array     |
| url | URL to share | string     |
| path | Path to share icons | string     |
| type | Filetype of share icons | string     |
| text | Additional share text | string     |

\* Required argument