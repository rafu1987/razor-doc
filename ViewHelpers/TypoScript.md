# TypoScript ViewHelper

## Example usage

```html
<razor:ts />
```

### Tag usage example

```html
<razor:ts />
```

### Inline usage example

```
{razor:ts(type:'NULL')}
```