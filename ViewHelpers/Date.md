# Date ViewHelper

## Example usage

```html
<razor:utility.date date="{f:format.date(format:'U' date:'{newsItem.datetime}')}" format="%d. %B %Y" />
```

### Tag usage example

```html
<razor:utility.date date="NULL" format="NULL" />
```

### Inline usage example

```
{razor:utility.date(date:'NULL', format: 'NULL')}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| date* | The date | string     |
| format*      | Date format | string   |

\* Required argument