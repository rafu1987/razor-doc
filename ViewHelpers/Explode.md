# Explode ViewHelper

## Example usage

```html
<razor:utility.explode content="This is\n content" />
```

### Tag usage example

```html
<razor:utility.explode content="NULL" />
```

### Inline usage example

```
{razor:utility.explode(content:'NULL')}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| content* | The content | string     |

\* Required argument