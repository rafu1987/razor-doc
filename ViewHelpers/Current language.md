# Current language ViewHelper

## Example usage

```html
<razor:utility.currentLang />
```

### Tag usage example

```html
<razor:utility.currentLang defaultLang="NULL" defaultLangIso="NULL" />
```

### Inline usage example

```
{razor:utility.currentLang(defaultLang:'NULL', defaultLangIso:'NULL')}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| defaultLang | Default language of your TYPO3 installation | string     |
| defaultLangIso | Default language code (ISO) of your TYPO3 installation  | string     |

\* Required argument