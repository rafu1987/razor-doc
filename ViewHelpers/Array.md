# Array ViewHelper

## Example usage

```html
<razor:utility.array obj="{0: 'foo', 1: 'bar'}" prop="{i.index}" />
```

### Tag usage example

```html
<razor:utility.array obj="{foo: 'bar'}" prop="[mixed]" />
```

### Inline usage example

```
{razor:utility.array(obj: {foo: 'bar'}, prop: [mixed])}
```

## Arguments

| Argument   | Description      | Type    |
| -----------| ---------------- | ------- |
| obj*       | The array        | array   |
| prop*      | The index        | mixed   |

\* Required argument