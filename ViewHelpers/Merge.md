# Merge ViewHelper

## Example usage

```html
<razor:utility.merge a="{foo: bar}" b="{bar: foo}" />
```

### Tag usage example

```html
<razor:utility.merge a="NULL" b="NULL" />
```

### Inline usage example

```
{razor:utility.merge(a:'NULL', b:'NULL')}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| a* | The first array | array     |
| b* | The second array | array     |

\* Required argument