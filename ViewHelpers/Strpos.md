# Strpos ViewHelper

## Example usage

```html
<razor:utility.strpos haystack="hello world" needle="world" />
```

### Tag usage example

```html
<razor:utility.strpos haystack="NULL" needle="NULL" />
```

### Inline usage example

```
{razor:utility.strpos(haytack:'NULL', needle:'NULL')}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| haytack* | The string to search in | string     |
| needle* | The string to search | string     |

\* Required argument