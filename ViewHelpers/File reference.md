# File Reference ViewHelper

## Example usage

```html
<razor:file.fileReference table="pages" field="media" uid="2" />
```

### Tag usage example

```html
<razor:file.fileReference table="NULL" field="NULL" uid="NULL" />
```

### Inline usage example

```
{razor:file.fileReference(table:'NULL', field:'NULL', uid:'NULL')}
```

## Arguments

| Argument   | Description                           | Type    |
| -----------| ------------------------------------- | ------- |
| table* | The table | string     |
| field* | The field | string     |
| uid* | The uid of the record | int     |

\* Required argument