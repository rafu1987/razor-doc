# Requirements

-   TYPO3 8.7.x
-   PHP 7.0.x - 7.2.x
-   MySQL 5.6.x
-   **Subdomain structure and not subfolder! (i.e.http://myproject.local)**
-   *node.js*
-   *gulp*
-   *yarn*
-   *Yeoman*